const http = require('http');
const fs = require('fs');
const path = require('path');
const url = require('url');
const { reset } = require('nodemon');
const port = 8000;
const PUBLIC_DIRECTORY = path.join(__dirname, 'public');

const renderHTML = (path, res) => {
  fs.readFile(path, (err, data) => {
    if(err){
      res.writeHead(404)
      res.write("PAGE NOT FOUND")
    }else{
      res.write(data);
    }
    res.end();
  });
};

http.createServer((req, res) => {
  let requesturl = req.url;
  switch(requesturl){
      case "/" || "":
        requesturl = "/index.html";
        break;
      case "/register":
        requesturl = "/register.html";
        break;
      case "/buy":
        requesturl = "/buy.html";
        break;
      default:
        requesturl = req.url;
        break;
    }
  const parseURL = url.parse(requesturl);
  const pathName = `${parseURL.pathname}`
  const extension = path.parse(pathName).ext;
  const absolutePath = path.join(PUBLIC_DIRECTORY, pathName);
  console.log(`extension`, extension);
  console.log(`absolute`, absolutePath);
  const mapContent = {
    ".css": "text/css",
    ".jpg": "image/jpeg",
    ".html": "text/html",
    ".js": "text/javascript"
  }

  fs.exists(absolutePath, (exist) => {
    if(!exist){
      res.writeHead(404);
      res.end("FILE NOT FOUND");
      return;
    }
  })

  fs.readFile(absolutePath, (err, data)  => {
    if(err){
      res.statusCode=500;
      res.end("FILE NOT FOUND");
      console.log(err);
    }else{
      res.setHeader('Content-Type', mapContent[extension] || "text/plain");
      res.end(data)
    }
  });

})

.listen(port, () => {
  console.log(`http://localhost:${port}`);
})


